# Master Thesis



## Topic

Modeling of road traffic in large urban networks using partial
differential equations

## Abstract

The following work, "Modeling of road traffic in large urban networks using partial differential
equations" deals with the issue of modeling urban traffic. The aim of the work is to understand
and test simulation algorithms and road traffic on large city networks. The first part of the thesis
is the theoretical part, which includes a review of road traffic simulation algorithms modeled with
partial differential equations, as well as the selection of an appropriate algorithm and its parameters.
Professional literature dealing with the modeling of flow, "shock waves" and intersections was used
to write the theoretical part. The second part is the implementation part, where the implementation
of the previously selected algorithms along with the GUI is described. The third part of the work is
to simulate and study the city network. The analysis includes the study of both individual parts of
the urban network and large urban networks. The final part of the work is conclusions. The study
managed to meet all the objectives and showed that using partial differential equations it is possible
to simulate models based on real city networks. Based on the research results, it was found that they
behave as expected, similar to the city networks on the basis of which they were modeled. There
were observed phenomena that can be observed in real urban networks, such as an increase in the
length of the passage due to the narrowing of the road along the route, the formation of repeating
areas of different density in the vicinity of intersections or the formation of traffic jams and gradual
saturation of the urban network.

Keywords: modeling, urban networks, differential equations, Lighthill-Whitham-Richards model,
simulation, traffic, implementation

## User Guide

The program and the simulation can be operated using the GUI. The GUI consists of two parts: the left one, which displays the map and the input signal, and the right one, which shows various options.<br /><br /><br />

<img src="/Photos/dodatek_a.png" alt="Alt text" title="Program GUI with initialized map." width="750">
<br />
Program GUI with initialized map.
<br />
<br />

GUI options:


1. Kliknij aby zacząć/zatrzymać (Click to start/stop) allows you to initialize the simulation or turn it off.

2. Wybierz mapę (Select a map) allows you to select a map from the options available in the drop-down menu, such as city, single intersection, perpendicular road grid, etc. The selected map will be visible in the left side of the GUI after pressing the button „Kliknij aby zacząć” (Click to start).
3. Pomniejsz (Zoom out) allows you to zoom out of the map.
4. Wybór wejścia (Input selection), this group of buttons and sliders in the GUI is used to set the input signal. In order to select an entrance, right-click in the map area on the arrow marking the entrance (i.e. reaching the road). The entry graph is visible in the upper right corner of the map area. Input signal parameters:

	- Wybierz gęstość wejścia (Select input density) allows you to select the density (or maximum density for variable signals) of the input signal.

	- Wybierz typ wejścia (Select the input type) is a drop-down menu where you can choose the type of output, the options are constant, sine, step and random.

	- Wybierz częstotliwość wejścia (Select the input frequency) is a slider with which you can select the frequency of the sine and step signals (it has no effect on other types of signals).

	- Ustaw wejście (Set input) assigns the input signal to the previously selected input.
	- Duplikuj wejście (Duplicate input) assigns a given input signal to all inputs in the simulation (scaling to their maximum parameters).


5. 	Zobacz historię (View history) opens a new window where you can display the history of density changes on a given road by entering the road number and clicking „Ustaw”(Set. By pressing the „Zapisz” (Save) button, graphs with history on selected roads are saved (up to four roads can be selected).

6. Zobacz różne wartości (View different values) similarly to the previous one, it opens a new window where, by entering the road number and clicking „Ustaw”(Set), you can display graphs of density, flow and speed on a given road. The „Zapisz”(Save) button saves the obtained graphs.
7. Pomoc (Help) opens a window with basic information on how to use the program.
8. Obliczanie czasu przejazdu (Travel time calculation), after selecting the box with this option, you can select the starting and ending roads by clicking on the arrow with the road number in the map area with the right mouse button. Below you get the duration of the trip in seconds.

In addition to these menu options, in the map area, double-clicking in the center of the map (20-80% of both height and width) zooms in on the map. However, clicking on the edges (up to 20\% of the width or height from the edge) moves the map.
	
## Programmer manual

The program was written in the Python programming language. The main library it uses is Tkinter, which is a library made for making GUIs. The program consists of a main class that consists of various methods and classes. These classes are **start\_input**, **crossroad\_macro** and **road**. These are classes containing information about input signals, intersections and individual roads, respectively. When each map is initialized, lists containing instances of these classes are created.<br />
The appearance of the main GUI can be changed in the **mainFrame** function, and the view of the windows containing the history and graphs of density, flow and speed. The initialization takes place in the **innit\_streets** function. To add other maps, create another option to choose from in the drop-down menu and add this option along with roads and intersections in this function.<br />
After the selected simulation is initialized, the program proceeds to the **run** function, which runs in a loop. In each iteration, the program calculates the values of individual road segments in the **one\_iteration** function, and then draws the given iteration in the **paint** function. The **one\_iteration** function references other functions such as **shortest\_path**, which calculates the shortest path using Dijkstra's algorithm, **supply\_demand**, which calculates values for roads entering and leaving an intersection , or **function\_S**, which calculates the values inside each path.<br />
The map is drawn using the **paint** function, the **return\_points** function determines the coordinates of road segments on the map, taking into account the map zoom and shift. The rectangles based on the received coordinates are then colored according to their density value as a percentage of the maximum value. The **draw\_path** function draws the shortest path, **paint\_crossroad\_connections** the connection in a given iteration at intersections, and the **draw\_input ** function draws it in the upper right corner. The **paint** function also draws all arrows, lights, etc.
Drawing in the **View history** and **View different values** windows is done in the **animate\_values** function, while using **save\_pic** these graphs are saved as pictures.

## Photos
1.<br />
<img src="/Photos/GUI_koncowe.png" alt="Alt text" title="Program GUI without initialized map." width="750">
<br />
Program GUI without initialized map.
<br /><br /><br />
2.<br />
<img src="/Photos/Historia_GUI.png" alt="Alt text" title="Window with historical data." width="750">
<br />
Window with historical data. The x axis refers to the iteration and y axis to the point on the road. Colors signify the density of road traffic in that given point on the road in given iteration.
<br /><br /><br />
3.<br />
<img src="/Photos/miasto_mapa.png" alt="Alt text" title="Map of the city in the program." width="750">
<br />
Map of the city in the program.

4.<br />
<img src="/Photos/wejscia.png" alt="Alt text" title="Screenshots of four possible inputs to a road." width="750">
<br />
Screenshots of four possible inputs to a road:

- [constant input ] 
- [sinusoidal input ] 
- [step input ] 
- [random input ] <br />
5.<br />
<img src="/Photos/symulacja_bez_obwodnicy.png" alt="Alt text" title="Draph shows the meassured impact of the road works on the length of road travel time." width="750">
<br />

Draph shows the meassured impact of the read works on the length of road travel time (red - without road works, blue - with road works).
<br />
6.<br />
<img src="/Photos/iteracje_skrzyzowania.png" alt="Alt text" title="Subsequent iterations of intersection." width="750">
<br />
Subsequent iterations of intersection.
